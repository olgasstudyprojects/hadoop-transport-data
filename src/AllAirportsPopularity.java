import common_data_structures.InputLine;
import common_data_structures.Pair;
import common_data_structures.TextArrayWritable;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.KeyValueTextInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;

import java.io.IOException;
import java.util.*;

public class AllAirportsPopularity {
    public static class AirportFlightsMap extends Mapper<LongWritable, Text, Text, IntWritable> {
        private final static IntWritable one = new IntWritable(1);
        private Text from = new Text();
        private Text to = new Text();

        @Override
        public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
            InputLine line = new InputLine(value);
            from.set(line.origin);
            to.set(line.destination);
            context.write(from, one);
            context.write(to, one);
        }
    }

    public static class AirportFlightsReducer extends Reducer<Text, IntWritable, Text, IntWritable> {
        @Override
        public void reduce(Text key, Iterable<IntWritable> values, Context context)
                throws IOException, InterruptedException {
            Integer result = 0;
            for (IntWritable val : values) {
                result += val.get();
            }
            context.write(key, new IntWritable(result));
        }
    }

    public static class AirportPopularityMapper extends Mapper<Text, Text, Text, IntWritable> {
        private TreeSet<Pair<Integer, String>> airportData =
                new TreeSet<Pair<Integer, String>>(Collections.<Pair<Integer,String>>reverseOrder());

        @Override
        public void map(Text key, Text value, Context context) throws IOException, InterruptedException {
            Integer popularity = Integer.parseInt(value.toString());
            airportData.add(new Pair<Integer, String>(popularity, key.toString()));
        }

        @Override
        protected void cleanup(Context context) throws IOException, InterruptedException {
            for (Pair<Integer, String> record : airportData) {
                context.write(new Text(record.second), new IntWritable(record.first));
            }
        }
    }

    public static void main(String[] args) throws Exception {
        Configuration conf = new Configuration();
        FileSystem fs = FileSystem.get(conf);
        Path tmpPath = new Path("./tmp");
        fs.delete(tmpPath, true);


        Job jobCount = Job.getInstance(conf, "airport-flights");
	jobCount.setJarByClass(AllAirportsPopularity.class);
        Job jobRate = Job.getInstance(conf, "airport-flights-ranking");
	jobRate.setJarByClass(AllAirportsPopularity.class);

        jobCount.setInputFormatClass(TextInputFormat.class);
        jobCount.setOutputKeyClass(Text.class);
        jobCount.setOutputValueClass(IntWritable.class);

        jobCount.setMapperClass(AirportFlightsMap.class);
        jobCount.setReducerClass(AirportFlightsReducer.class);
        FileInputFormat.addInputPath(jobCount, new Path(args[0]));
        FileOutputFormat.setOutputPath(jobCount, tmpPath);
        jobCount.waitForCompletion(true);

        jobRate.setOutputKeyClass(Text.class);
        jobRate.setOutputValueClass(TextArrayWritable.class);
        jobRate.setInputFormatClass(KeyValueTextInputFormat.class);
        jobRate.setOutputFormatClass(TextOutputFormat.class);

        jobRate.setMapOutputValueClass(TextArrayWritable.class);

        jobRate.setMapperClass(AirportPopularityMapper.class);
        jobRate.setNumReduceTasks(0);

        FileInputFormat.setInputPaths(jobRate, tmpPath);
        FileOutputFormat.setOutputPath(jobRate, new Path(args[1]));

        System.exit(jobRate.waitForCompletion(true) ? 0 : 1);
    }
}
