import common_data_structures.InputLine;
import common_data_structures.RouteParts;
import common_data_structures.TextArrayWritable;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.log4j.Logger;


import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

public class TomsTravelling {
    private static String targetYear = "2008";
    private static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    public static class DestinationsMap extends Mapper<LongWritable, Text, Text, TextArrayWritable> {
        private HashMap<String, RouteParts> flightsByFirstDest = new HashMap<>();
	
	private Logger logger = Logger.getLogger(DestinationsMap.class);

        @Override
	protected void setup(Context context) {
	    try {
		super.setup(context);
		logger.info("Initializing Tom's travelling for year " + targetYear);
	    } catch(Exception e) {
		logger.info(e.getMessage());
	    }    
        }

        @Override
        public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
            InputLine line = new InputLine(value);
            if(!line.year.equals(targetYear) || line.arrDelayMinutes == null) { return; }
            //logger.info("record for year " + targetYear + " found!");
	    if( line.depTime.compareTo("1200") > 0) {
                //second leg of a journey
                String previousFlightKey = line.origin + "," + dateFormat.format(twoDaysBefore(line.flightDate));
                extendRouteParts(previousFlightKey, false, line);
            } else {
                //first leg of a journey
                String firstFlightKey = line.destination + "," + dateFormat.format(line.flightDate);
                extendRouteParts(firstFlightKey, true, line);
            }
        }

        @Override
        protected void cleanup(Context context) throws IOException, InterruptedException {
            System.out.println(targetYear);
	    Set<Map.Entry<String, RouteParts>> entries = flightsByFirstDest.entrySet();
            Text route = new Text();
            TextArrayWritable routeData = new TextArrayWritable();
            for (Map.Entry<String, RouteParts> entry : entries) {
                RouteParts routeParts = entry.getValue();
                ArrayList<InputLine> allOriginsToThisDest = routeParts.firstLegs;
                ArrayList<InputLine> allHopsFromThisDest = routeParts.secondLegs;
                for(InputLine firstFlight : allOriginsToThisDest) {
                    for(InputLine secondFlight : allHopsFromThisDest) {
                        String travelRouteKey = firstFlight.origin + "\t" + firstFlight.destination + "\t" +
                                secondFlight.destination + "\t" + dateFormat.format(firstFlight.flightDate);
                        Float totalArrivalDelay = firstFlight.arrDelayMinutes + secondFlight.arrDelayMinutes;
                        String[] values = new String[] {
                                firstFlight.depTime,
                                firstFlight.uniqueCarrier,
                                dateFormat.format(secondFlight.flightDate),
                                secondFlight.depTime,
                                secondFlight.uniqueCarrier,
                                totalArrivalDelay.toString()
                        };
                        route.set(travelRouteKey);
                        routeData.set(values);
                        context.write(route, routeData);
                    }
                }
            }
        }

        private void extendRouteParts(String flightKey, Boolean firstLeg, InputLine line) {
            if(!flightsByFirstDest.containsKey(flightKey)) {
                RouteParts routeParts = new RouteParts();
                routeParts.firstLegs =  new ArrayList<>();
                routeParts.secondLegs =  new ArrayList<>();
                flightsByFirstDest.put(flightKey, routeParts);
            }
            if(firstLeg) {
                flightsByFirstDest.get(flightKey).firstLegs.add(line);
            } else {
                flightsByFirstDest.get(flightKey).secondLegs.add(line);
            }
        }

        private static Date twoDaysBefore(Date date) {
            Calendar c = Calendar.getInstance();
            c.setTime(date);
            c.add(Calendar.DATE, -2);
            return c.getTime();
        }
    }

    public static class RoutesReduce extends Reducer<Text, TextArrayWritable, Text, TextArrayWritable> {
        @Override
        public void reduce(Text key, Iterable<TextArrayWritable> values, Context context)
                throws IOException, InterruptedException {
            Float minDelay = Float.MAX_VALUE;
            TextArrayWritable initialRoute = new TextArrayWritable();
            TextArrayWritable fastestRoute = new TextArrayWritable();
            for(TextArrayWritable route : values) {
                String[] routeArr = route.toStrings();
                Float delay = Float.parseFloat(routeArr[5]);
                if(delay < minDelay) {
                    minDelay = delay;
                    fastestRoute = route;
                }
            }
            if(fastestRoute != initialRoute) {
                context.write(key, fastestRoute);
            }
        }
    }

    public static void main(String[] args) throws Exception {
        Job job = Job.getInstance(new Configuration(), "toms-travelling");
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(TextArrayWritable.class);
	    job.setJarByClass(TomsTravelling.class);

        job.setMapperClass(DestinationsMap.class);
        job.setCombinerClass(RoutesReduce.class);
        job.setReducerClass(RoutesReduce.class);
        job.setMapOutputValueClass(TextArrayWritable.class);

        FileInputFormat.setInputPaths(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));

        System.exit(job.waitForCompletion(true) ? 0 : 1);
    }
}
