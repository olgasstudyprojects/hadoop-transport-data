import common_data_processors.AirportStatisticsMapper;
import common_data_processors.PartAndTotalReducer;
import common_data_structures.InputLine;
import common_data_structures.PartAndTotalWritable;
import common_data_structures.TextArrayWritable;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.KeyValueTextInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;

import java.io.IOException;

public class CarriersAirportsDeparture {
    public static class AirportCarriersMap extends Mapper<LongWritable, Text, Text, PartAndTotalWritable> {
        private Text airportCarrier = new Text();

        @Override
        public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
            InputLine line = new InputLine(value);

            airportCarrier.set(line.origin + "," + line.uniqueCarrier);
            PartAndTotalWritable output = new PartAndTotalWritable();
            output.totalSum = 1;
            if(line.depOnTime) {
                output.partSum = 1;
            }
            context.write(airportCarrier, output);
        }
    }

    public static void main(String[] args) throws Exception {
        Configuration conf = new Configuration();
        FileSystem fs = FileSystem.get(conf);
        Path tmpPath = new Path("./tmp");
        fs.delete(tmpPath, true);


        Job jobCount = Job.getInstance(conf, "ontime-airport-carriers");
        jobCount.setJarByClass(CarriersAirportsDeparture.class);
	Job jobRate = Job.getInstance(conf, "ontime-airport-carriers-rating");
	jobRate.setJarByClass(CarriersAirportsDeparture.class);

        jobCount.setInputFormatClass(TextInputFormat.class);
        jobCount.setOutputKeyClass(Text.class);
        jobCount.setOutputValueClass(PartAndTotalWritable.class);

        jobCount.setMapperClass(AirportCarriersMap.class);
        jobCount.setReducerClass(PartAndTotalReducer.class);
        FileInputFormat.addInputPath(jobCount, new Path(args[0]));
        FileOutputFormat.setOutputPath(jobCount, tmpPath);
        jobCount.waitForCompletion(true);

        jobRate.setOutputKeyClass(Text.class);
        jobRate.setOutputValueClass(TextArrayWritable.class);
        jobRate.setInputFormatClass(KeyValueTextInputFormat.class);
        jobRate.setOutputFormatClass(TextOutputFormat.class);

        jobRate.setMapOutputValueClass(TextArrayWritable.class);

        jobRate.setMapperClass(AirportStatisticsMapper.class);
        jobRate.setNumReduceTasks(0);

        FileInputFormat.setInputPaths(jobRate, tmpPath);
        FileOutputFormat.setOutputPath(jobRate, new Path(args[1]));

        System.exit(jobRate.waitForCompletion(true) ? 0 : 1);
    }
}
