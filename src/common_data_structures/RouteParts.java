package common_data_structures;


import java.util.ArrayList;

public class RouteParts {
    public ArrayList<InputLine> firstLegs = new ArrayList<InputLine>();
    public ArrayList<InputLine> secondLegs = new ArrayList<InputLine>();
}
