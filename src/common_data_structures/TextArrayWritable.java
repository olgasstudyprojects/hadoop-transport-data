package common_data_structures;// adapted from https://github.com/xldrx/mapreduce_examples/ (CCA class)

import org.apache.hadoop.io.ArrayWritable;
import org.apache.hadoop.io.Text;

public class TextArrayWritable extends ArrayWritable {
    private String[] strings;
    public TextArrayWritable() {
        super(Text.class);
    }

    public TextArrayWritable(String[] strings) {
        super(Text.class);
        set(strings);
    }


    public void set(String[] strings) {
        this.strings = strings;
        Text[] texts = new Text[strings.length];
        for (int i = 0; i < strings.length; i++) {
            texts[i] = new Text(strings[i]);
        }
        set(texts);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        String[] strings = this.toStrings();
        for (int i = 0; i < strings.length; i++) {
            sb.append(strings[i]);
            if(i < strings.length - 1) {
               sb.append("\t");
            }
        }
        return sb.toString();
    }
}
