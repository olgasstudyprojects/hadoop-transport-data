package common_data_structures;
import org.apache.hadoop.io.Text;

import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class InputLine {
    public String airlineID;
    public String uniqueCarrier;
    public String origin;
    public String destination;
    public Date flightDate;
    public String year;
    public String dayOfWeek;
    public String depTime;
    public Float depDelayMinutes = null;
    public Boolean depOnTime = false;
    public String arrTime;
    public Float arrDelayMinutes = null;
    public Boolean arrOnTime = false;

    public InputLine(Text value) {
        String line = value.toString().replace(System.lineSeparator(), "");
        String[] values = line.split("\\|", -1);
        this.airlineID = values[0];
        this.uniqueCarrier = values[1];
        this.origin = values[2];
        this.destination = values[3];

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        try {
            this.flightDate = sdf.parse(values[4]);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        
        this.year = values[5];
        this.dayOfWeek = values[6];
        this.depTime = values[7];
        if(!values[8].equals("")){
            this.depDelayMinutes = Float.parseFloat(values[8]);
        }
        this.depOnTime = values[9].equals("0.00");
        this.arrTime = values[10];
        if(!values[11].equals("")){
            this.arrDelayMinutes = Float.parseFloat(values[11]);
        }
        this.arrOnTime = values[12].equals("0.00");
    }
}
