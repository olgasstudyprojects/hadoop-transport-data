package common_data_structures;

import org.apache.hadoop.io.Writable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public class PartAndTotalWritable implements Writable {
    public int partSum = 0;
    public int totalSum = 0;

    public PartAndTotalWritable() {}

    public PartAndTotalWritable(int partSum, int totalSum) {
        this.partSum = partSum;
        this.totalSum = totalSum;
    }

    @Override
    public void readFields(DataInput in) throws IOException {
        partSum = in.readInt();
        totalSum = in.readInt();
    }

    @Override
    public void write(DataOutput out) throws IOException {
        out.writeInt(partSum);
        out.writeInt(totalSum);
    }

    public void merge(PartAndTotalWritable other) {
        this.partSum += other.partSum;
        this.totalSum += other.totalSum;
    }

    @Override
    public String toString() {
        return this.partSum + "," + this.totalSum;
    }
}