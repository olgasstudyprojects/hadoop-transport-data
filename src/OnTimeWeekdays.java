import java.io.IOException;
import java.util.TreeSet;

import common_data_processors.PartAndTotalReducer;
import common_data_processors.TopTableReducer;
import common_data_structures.InputLine;
import common_data_structures.Pair;
import common_data_structures.PartAndTotalWritable;
import common_data_structures.TextArrayWritable;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.conf.*;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.*;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.KeyValueTextInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;

public class OnTimeWeekdays {

    public static class WeekdayOnTimeMap extends Mapper<LongWritable, Text, Text, PartAndTotalWritable> {
        private Text weekday = new Text();

        public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
            InputLine line = new InputLine(value);
            weekday.set(line.dayOfWeek);

            PartAndTotalWritable output = new PartAndTotalWritable();
            output.totalSum = 1;
            if(line.arrOnTime) {
                output.partSum = 1;
            }
            context.write(weekday, output);
        }
    }

    public static class TopWeekdaysMap extends Mapper<Text, Text, NullWritable, TextArrayWritable> {
        private TreeSet<Pair<Float, String>> countToDayMap = new TreeSet<Pair<Float, String>>();

        @Override
        public void map(Text key, Text value, Context context)
                throws IOException, InterruptedException {
            String[] partAndTotal = value.toString().split(",");
            Integer onTime = Integer.parseInt(partAndTotal[0]);
            Integer total = Integer.parseInt(partAndTotal[1]);
            Float rate = (onTime.floatValue() / total.floatValue()) * 100;
            String word = key.toString();

            countToDayMap.add(new Pair<Float, String>(rate, word));
        }

        @Override
        protected void cleanup(Context context) throws IOException, InterruptedException {
            for (Pair<Float, String> item : countToDayMap) {
                String[] strings = {item.second, item.first.toString()};
                TextArrayWritable val = new TextArrayWritable(strings);
                context.write(NullWritable.get(), val);
            }
        }
    }

    public static void main(String[] args) throws Exception {
        Configuration conf = new Configuration();
        FileSystem fs = FileSystem.get(conf);
        Path tmpPath = new Path("./tmp");
        fs.delete(tmpPath, true);


        Job jobCount = Job.getInstance(conf, "ontime-weekdays");
	jobCount.setJarByClass(OnTimeWeekdays.class);
        Job jobRate = Job.getInstance(conf, "ontime-weekdays-rating");
	jobRate.setJarByClass(OnTimeWeekdays.class);

        jobCount.setInputFormatClass(TextInputFormat.class);
        jobCount.setOutputKeyClass(Text.class);
        jobCount.setOutputValueClass(PartAndTotalWritable.class);

        jobCount.setMapperClass(WeekdayOnTimeMap.class);
        jobCount.setReducerClass(PartAndTotalReducer.class);
        FileInputFormat.addInputPath(jobCount, new Path(args[0]));
        FileOutputFormat.setOutputPath(jobCount, tmpPath);
        jobCount.waitForCompletion(true);


        jobRate.setOutputKeyClass(Text.class);
        jobRate.setOutputValueClass(FloatWritable.class);
        jobRate.setInputFormatClass(KeyValueTextInputFormat.class);
        jobRate.setOutputFormatClass(TextOutputFormat.class);

        jobRate.setMapOutputKeyClass(NullWritable.class);
        jobRate.setMapOutputValueClass(TextArrayWritable.class);

        jobRate.setMapperClass(TopWeekdaysMap.class);
        jobRate.setReducerClass(TopTableReducer.class);
        jobRate.setNumReduceTasks(1);

        FileInputFormat.setInputPaths(jobRate, tmpPath);
        FileOutputFormat.setOutputPath(jobRate, new Path(args[1]));

        System.exit(jobRate.waitForCompletion(true) ? 0 : 1);
    }

}
