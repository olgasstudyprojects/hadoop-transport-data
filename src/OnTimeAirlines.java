import java.io.IOException;
import java.util.TreeSet;

import common_data_processors.BayesianRating;
import common_data_processors.PartAndTotalReducer;
import common_data_processors.TopTableReducer;
import common_data_structures.InputLine;
import common_data_structures.Pair;
import common_data_structures.PartAndTotalWritable;
import common_data_structures.TextArrayWritable;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.conf.*;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.*;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.KeyValueTextInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;

public class OnTimeAirlines {

    public static class AirlineOnTimeMap extends Mapper<LongWritable, Text, Text, PartAndTotalWritable> {
        private Text airline = new Text();

        public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
            InputLine line = new InputLine(value);

            airline.set(line.airlineID);
            PartAndTotalWritable output = new PartAndTotalWritable();
            output.totalSum = 1;
            if(line.arrOnTime) {
                output.partSum = 1;
            }
            context.write(airline, output);
        }
    }

    public static class TopAirlinesMap extends Mapper<Text, Text, NullWritable, TextArrayWritable> {
        private TreeSet<Pair<Float, String>> countToAirlineIdMap = new TreeSet<Pair<Float, String>>();
        // to be adjusted according to the dataset size
        private final static Integer INITIAL_FLIGHTS_THRESHOLD = 100000;
        // based on statistics received from OnTimeWeekdays.java: on average, about 78% of flights
        // 1) happen at all
        // 2) arrive on time
        private BayesianRating weightedRating = new BayesianRating(new Float(0.78), INITIAL_FLIGHTS_THRESHOLD);

        @Override
        public void map(Text key, Text value, Context context) throws IOException, InterruptedException {
            String[] partAndTotal = value.toString().split(",");
            Integer onTime = Integer.parseInt(partAndTotal[0]);
            Integer total = Integer.parseInt(partAndTotal[1]);
            Float rate = weightedRating.weightedRating(onTime, total) * 100;

            String airlineId = key.toString();

            countToAirlineIdMap.add(new Pair<Float, String>(rate, airlineId));

            if (countToAirlineIdMap.size() > 10) {
                countToAirlineIdMap.remove(countToAirlineIdMap.first());
            }
        }

        @Override
        protected void cleanup(Context context) throws IOException, InterruptedException {
            for (Pair<Float, String> item : countToAirlineIdMap) {
                String[] strings = {item.second, item.first.toString()};
                TextArrayWritable val = new TextArrayWritable(strings);
                context.write(NullWritable.get(), val);
            }
        }
    }

    public static void main(String[] args) throws Exception {
        Configuration conf = new Configuration();
        FileSystem fs = FileSystem.get(conf);
        Path tmpPath = new Path("./tmp");
        fs.delete(tmpPath, true);


        Job jobCount = Job.getInstance(conf, "ontime-airlines");
        jobCount.setJarByClass(OnTimeAirlines.class);
	Job jobRate = Job.getInstance(conf, "ontime-airlines-rating");
	jobRate.setJarByClass(OnTimeAirlines.class);

        jobCount.setInputFormatClass(TextInputFormat.class);
        jobCount.setOutputKeyClass(Text.class);
        jobCount.setOutputValueClass(PartAndTotalWritable.class);

        jobCount.setMapperClass(AirlineOnTimeMap.class);
        jobCount.setReducerClass(PartAndTotalReducer.class);
        FileInputFormat.addInputPath(jobCount, new Path(args[0]));
        FileOutputFormat.setOutputPath(jobCount, tmpPath);
        jobCount.waitForCompletion(true);

        jobRate.setOutputKeyClass(Text.class);
        jobRate.setOutputValueClass(FloatWritable.class);
        jobRate.setInputFormatClass(KeyValueTextInputFormat.class);
        jobRate.setOutputFormatClass(TextOutputFormat.class);

        jobRate.setMapOutputKeyClass(NullWritable.class);
        jobRate.setMapOutputValueClass(TextArrayWritable.class);

        jobRate.setMapperClass(TopAirlinesMap.class);
        jobRate.setReducerClass(TopTableReducer.class);
        jobRate.setNumReduceTasks(1);

        FileInputFormat.setInputPaths(jobRate, tmpPath);
        FileOutputFormat.setOutputPath(jobRate, new Path(args[1]));

        System.exit(jobRate.waitForCompletion(true) ? 0 : 1);
    }

}
