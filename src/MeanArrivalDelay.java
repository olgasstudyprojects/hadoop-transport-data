import common_data_structures.InputLine;
import common_data_structures.TextArrayWritable;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.IOException;

public class MeanArrivalDelay {
    public static class ArrivalDelayMap extends Mapper<LongWritable, Text, Text, FloatWritable> {
        private Text fromTo = new Text();

        @Override
        public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
            InputLine line = new InputLine(value);
            fromTo.set(line.origin + "," + line.destination);
            if(line.arrDelayMinutes != null) {
                context.write(fromTo, new FloatWritable(line.arrDelayMinutes));
            }
        }
    }

    public static class ArrivalDelayReduce extends Reducer<Text, FloatWritable, Text, TextArrayWritable> {

        @Override
        public void reduce(Text key, Iterable<FloatWritable> values, Context context)
                throws IOException, InterruptedException {
            Float sum = new Float(0.0);
            Integer total = 0;
            for (FloatWritable val : values) {
                sum += val.get();
                total +=1;
            }
            Float mean = sum / total.floatValue();

            String[] fromTo = key.toString().split(",");
            Text from = new Text(fromTo[0]);
            TextArrayWritable resultLine = new TextArrayWritable(new String[] {fromTo[1], mean.toString()});
            context.write(from, resultLine);
        }
    }

    public static void main(String[] args) throws Exception {

        Job job = Job.getInstance(new Configuration(), "mean-arrival-delay");
        job.setJarByClass(MeanArrivalDelay.class);
	job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(TextArrayWritable.class);

        job.setMapperClass(ArrivalDelayMap.class);
        job.setReducerClass(ArrivalDelayReduce.class);
        job.setMapOutputValueClass(FloatWritable.class);
        job.setNumReduceTasks(1);

        FileInputFormat.setInputPaths(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));

        job.setJarByClass(MeanArrivalDelay.class);
        System.exit(job.waitForCompletion(true) ? 0 : 1);
    }
}
