package common_data_processors;// For the explanation of the formula, see http://fulmicoton.com/posts/bayesian_rating/

public class BayesianRating {
    private Float initialProbability;
    private Integer eventsThreshold;

    public BayesianRating(Float initialProbability, Integer eventsThreshold) {
        this.eventsThreshold = eventsThreshold;
        this.initialProbability = initialProbability;
    }

    public Float weightedRating(Integer ratedEventsNumber, Integer totalEventsNumber) {
        Integer totalWithThreshold = eventsThreshold + totalEventsNumber;
        Float initialRating = initialProbability * eventsThreshold;
        return (initialRating + ratedEventsNumber.floatValue()) /
                totalWithThreshold.floatValue();
    }

}
