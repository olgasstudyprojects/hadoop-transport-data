package common_data_processors;

import common_data_structures.Pair;
import common_data_structures.TextArrayWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;
import java.util.*;

public class AirportStatisticsMapper extends Mapper<Text, Text, Text, TextArrayWritable> {
    private HashMap<String, TreeSet> airportData = new HashMap<String, TreeSet>();
    // to be adjusted according to the dataset size
    private final static Integer INITIAL_FLIGHTS_THRESHOLD = 1000;
    // based on statistics received from OnTimeWeekdays.java: on average, about 84% of flights
    // 1) happen at all
    // 2) depart on time
    private BayesianRating weightedRating = new BayesianRating(new Float(0.84), INITIAL_FLIGHTS_THRESHOLD);

    @Override
    public void map(Text key, Text value, Context context) throws IOException, InterruptedException {
        String[] partAndTotal = value.toString().split(",");
        String[] airportWithField = key.toString().split(",");

        Integer onTime = Integer.parseInt(partAndTotal[0]);
        Integer total = Integer.parseInt(partAndTotal[1]);
        String airport = airportWithField[0];
        String ratingField = airportWithField[1];

        TreeSet countToFieldMap = airportData.get(airport);
        if(countToFieldMap == null) {
            countToFieldMap = new TreeSet<Pair<Float, String>>(Collections.reverseOrder());
        }

        Float rate = weightedRating.weightedRating(onTime, total) * 100;

        countToFieldMap.add(new Pair<Float, String>(rate, ratingField));

        if (countToFieldMap.size() > 10) {
            countToFieldMap.remove(countToFieldMap.last());
        }
        airportData.put(airport, countToFieldMap);
    }

    @Override
    protected void cleanup(Context context) throws IOException, InterruptedException {
        Set<Map.Entry<String, TreeSet>> entries = airportData.entrySet();
        for (Map.Entry<String, TreeSet> entry : entries) {
            String airport = entry.getKey();
            TreeSet<Pair<Float, String>> ratings = (TreeSet<Pair<Float, String>>)entry.getValue();
            for (Pair<Float, String> item : ratings) {
                String[] strings = {item.second, item.first.toString()};
                TextArrayWritable val = new TextArrayWritable(strings);
                context.write(new Text(airport), val);
            }
        }
    }
}