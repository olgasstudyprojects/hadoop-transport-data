package common_data_processors;

import common_data_structures.TextArrayWritable;
import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

public class TopTableReducer extends Reducer<NullWritable, TextArrayWritable, Text, FloatWritable> {
    @Override
    public void reduce(NullWritable key, Iterable<TextArrayWritable> values, Context context)
            throws IOException, InterruptedException {
        for (TextArrayWritable val: values) {
            Text[] pair= (Text[]) val.toArray();
            Text airlineId= pair[0];
            FloatWritable value = new FloatWritable(Float.parseFloat(pair[1].toString()));
            context.write(airlineId, value);
        }
    }
}