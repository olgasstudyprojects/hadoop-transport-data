package common_data_processors;

import common_data_structures.PartAndTotalWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

public class PartAndTotalReducer extends Reducer<Text, PartAndTotalWritable, Text, PartAndTotalWritable> {

    public void reduce(Text key, Iterable<PartAndTotalWritable> values, Context context)
            throws IOException, InterruptedException {
        PartAndTotalWritable result = new PartAndTotalWritable();
        for (PartAndTotalWritable val : values) {
            result.merge(val);
        }
        context.write(key, result);
    }
}
